#!/bin/bash

HERE=$(dirname $(dirname $(readlink -fm $0)))

function detect_ramdisk_compression() {
    source "${HERE}/deviceinfo"

    case "${deviceinfo_ramdisk_compression:=gzip}" in
        gzip)
            COMPRESSION_CMD="gzip -9"
            ;;
        lz4)
            COMPRESSION_CMD="lz4 -l -9"
            ;;
        *)
            echo "Unsupported deviceinfo_ramdisk_compression value: '$deviceinfo_ramdisk_compression'"
            exit 1
            ;;
    esac
}

function gzip_compress_ramdisk() {
    RAMDISK=$(realpath $1)
    source "${HERE}/deviceinfo"

    detect_ramdisk_compression

    if [ "$deviceinfo_ramdisk_compression" != "gzip" ]; then
        gzip -dc "$RAMDISK" | $COMPRESSION_CMD > "${RAMDISK}.${deviceinfo_ramdisk_compression}"
        RAMDISK="${RAMDISK}.${deviceinfo_ramdisk_compression}"
    fi
}

function ramdisk_overlay_image() {
    RAMDISK=$(realpath $1)
    TMPDOWN=$(realpath $2)

    detect_ramdisk_compression

    if [ -d "$HERE/ramdisk-overlay" ]; then
        cp "$RAMDISK" "${RAMDISK}-merged"
        RAMDISK="${RAMDISK}-merged"
        cd "$HERE/ramdisk-overlay"
        find . | cpio -o -H newc | $COMPRESSION_CMD >> "$RAMDISK"

        # Restore unoverlayed recovery ramdisk
        if [ -f "$HERE/ramdisk-overlay/ramdisk-recovery.img" ] && [ -f "$TMPDOWN/ramdisk-recovery.img-original" ]; then
            mv "$TMPDOWN/ramdisk-recovery.img-original" "$HERE/ramdisk-overlay/ramdisk-recovery.img"
        fi
    fi
}

function detect_kernel_image_type() {
    TMPDOWN=$(realpath $1)
    KERNEL_OBJ=$(realpath $2)
    source "${HERE}/deviceinfo"

    case "$deviceinfo_arch" in
        aarch64*) ARCH="arm64" ;;
        arm*) ARCH="arm" ;;
        x86_64) ARCH="x86_64" ;;
        x86) ARCH="x86" ;;
    esac

    if [ -n "$deviceinfo_kernel_image_name" ]; then
        KERNEL="$KERNEL_OBJ/arch/$ARCH/boot/$deviceinfo_kernel_image_name"
    else
        # Autodetect kernel image name for boot.img
        if [ "$deviceinfo_bootimg_header_version" -ge 2 ]; then
            IMAGE_LIST="Image.gz Image"
        else
            IMAGE_LIST="Image.gz-dtb Image.gz Image"
        fi

        for image in $IMAGE_LIST; do
            if [ -e "$KERNEL_OBJ/arch/$ARCH/boot/$image" ]; then
                KERNEL="$KERNEL_OBJ/arch/$ARCH/boot/$image"
                break
            fi
        done
    fi
}